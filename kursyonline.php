<?php
/*
 Template Name: Kursy online
 */
?>
<?php
get_header(); ?>
<main id="course-all" role="main">
    <div class="wrapper">

        <?php
// Protect against arbitrary paged values
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
 
$args = array(
    'post_type' => 'kursyonline',
    'post_status'=>'publish',

    'posts_per_page' => 10,
    'paged' => $paged,
);

$the_query = new WP_Query($args);
?>

        <?php if ( $the_query->have_posts() ) : ?>

        <section style="background-image: url(<?php the_field('grafika_w_tle_tekstu_course') ?>)" class="welcome-text">
            <div class="container">
                <div class="row">
                    <div data-aos="fade-right" data-aos-duration="1500" class="col-lg-6">
                        <div class="area-titleAndBull">
                            <h1 class="title-page-courses"><?php the_title(); ?></h1>
                            <img src="<?php the_field('ikona_obok_tytulu_course') ?>" alt="" class="bull-courdes-title">
                        </div>
                        <div class="text-area-description-course">
                            <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                the_content();
                endwhile; else: ?>
                            <p>Przepraszamy, brak wpisanej treści w opisie</p>
                            <?php endif; ?>
                            <a href="<?php the_field('link_przycisku_pod_tekstem_glownym') ?>"
                                class="btn-siesta-red"><?php the_field('tekst_przycisku_pod_tekstem_glownym') ?></a>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div style="background-image: url('<?php the_field('zdjecie_obok_tekstu_glownego_course') ?>')"
                            class="img-area-description-course">
                            <!-- <img src="<?php the_field('zdjecie_obok_tekstu_glownego_course') ?>" alt=""> -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="scrollArea">
                <a href="#section-choose-course-online"><img
                        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icon/scroll_icon.svg" alt=""
                        class="img-scroll"></a>
            </div>
        </section>
        <section id="section-choose-course-online" data-aos="zoom-in" data-aos-duration="1500" class="choose-courses">
            <div class="container">
                <h2 class="title-section-choose-course">
                    <?php the_field('title-section-choose-course') ?>
                </h2>
                <div class="row">


                    <?php
            $taxonomy = 'online';
            $terms = get_terms($taxonomy, $args = array(
              'hide_empty' => false,
            ));

            ?>

                    <?php foreach( $terms as $term ) :
            $cat_image = get_field('miniatura_kategorii_custom_post', $term);

            ?>
                    <div class="col-md-6 col-lg-4">
                        <a class="image-nav-block" id="post-<?php the_ID(); ?>"
                            href="<?php echo get_term_link($term->slug, $taxonomy); ?>">
                            <div class="flag-language-block">
                                <img src="<?php echo $cat_image; ?>" alt="">
                                <h3 class="title-overlay">
                                    <?php echo $term->name; ?>
                                </h3>
                            </div>
                        </a>
                    </div>


                    <?php endforeach;?>





                </div>




                <?php endif; ?>


            </div>
        </section>




















    </div> <!-- end wrapper -->
</main><!-- .site-main -->
<?php get_footer(); ?>