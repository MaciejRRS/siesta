<?php
/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */ ?>
<div class="row">
    <?php if ( $query->have_posts() )
{
	?>



    <?php
	while ($query->have_posts())
	{
		$query->the_post();
		
		?>

    <div class="col-lg-6 col-xl-4">

        <div class="block-lang-filter">
            <a href="<?php the_permalink(); ?>">

                <?php 
				if ( has_post_thumbnail() ) { ?>
                <div class="container-img">

                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo '<div style="background: url('. $url.');" class="img-post-thumb">'; ?>
                </div>
                <div class="overly">
                    <div class="text">
                        <?php echo mb_strimwidth( get_the_excerpt(), 0, 200, '...' ); ?>
                    </div>
                </div>
        </div>


        <?php } else { ?>


        <div class="container-img">
            <div style="background: url(<?php the_field('alternative-img-course','option'); ?>);"
                class="img-post-thumb">;
            </div>
            <div class="overly">
                <div class="text">
                    <?php echo mb_strimwidth( get_the_excerpt(), 0, 200, '...' ); ?>
                </div>
            </div>
        </div>
        <?php } ?>



        <h2><?php the_title(); ?></h2>

        <div class="btn-more-filter-area">
            <div class="btn-red-more"><?php the_field('name_button_more_filter','option') ?></div>
        </div>
        </a>
    </div>
</div>






<?php
	}
    ?>
</div>
Page <?php echo $query->query['paged']; ?> of <?php echo $query->max_num_pages; ?><br />

<div class="pagination">

    <div class="nav-previous"><?php next_posts_link( 'Older posts', $query->max_num_pages ); ?></div>
    <div class="nav-next"><?php previous_posts_link( 'Newer posts' ); ?></div>
    <?php
            /* example code for using the wp_pagenavi plugin */
            
			if (function_exists('wp_pagenavi'))
			{
				echo "<br />";
				wp_pagenavi( array( 'query' => $query ) );
			}
		?>
</div>
<?php
}
else
{
	echo "Brak wyników";
}


?>