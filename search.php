<?php
/**
 * The template for displaying Search Results pages.
 */
get_header('pages') ?>

<div style="background-image:linear-gradient(to bottom, rgba(0, 0, 0, 0.65), rgba(0, 0, 0, 0.3)), url(<?php the_field('bg-search', 'option') ?>);"
    class="breadcrumb-bg">
    <div class="container-main">
        <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
            <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
        }
    ?>
        </nav>

        <div class="bg-title-breadcrumb">
            <h2>404</h2>
        </div>
    </div>
</div>


<section class="about-us">
    <div class="container-main">

        <?php if ( have_posts() ) : ?>
        <div class="row">
            <div class="col-md-12">
                <header class="page-header-search">
                    <h1 class="page-title text-center">
                        <?php printf( __( '%s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?>
                    </h1>
                </header>
            </div>
        </div><!-- end row -->


        <div class="row">
            <div class="col-md-12 my-4">
                <?php /* Start the Loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <ul class="search-list my-4">
                    <li>
                        <h2><a
                                href="<?php the_permalink(); ?>"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></a>
                        </h2>

                        <p class="search-text"><?php echo wp_trim_words( get_the_content(), 16, '...' ); ?>
                    </li>
                </ul>


                <?php endwhile; ?>

                <!-- blok tekstu -->

                <?php else : ?>

                <header class="page-header-search">
                    <h2 class="min-400 text-center"><?php the_field('brak_wynikow_tekst', 'option') ?></h2>
                </header>

                <?php endif; ?>
            </div>
        </div>
    </div><!-- #container-->
</section><!-- search-area -->

<?php get_footer(); ?>