<footer class="footer">
    <section class="color-footer">
        <div class="container">

            <div class="row">
                <?php
if( have_rows('lista_kolumn_footer', 'option') ):

    // Loop through rows.
    while( have_rows('lista_kolumn_footer', 'option') ) : the_row(); ?>
                <div class="col-md-4">
                    <div class="header-footer">
                        <h3><?php the_sub_field('header-footer-1') ?></h3>
                    </div>
                    <ul class="list-link-footer">
                        <?php
if( have_rows('lista_linkow_footer') ):

    // Loop through rows.
    while( have_rows('lista_linkow_footer') ) : the_row(); ?>
                        <li><a
                                href="<?php the_sub_field('link_item_footer') ?>"><?php the_sub_field('name_link_item_footer') ?></a>
                        </li>
                        <?php  // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
                    </ul>


                </div>
                <?php  // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
                <div class="col-md-4 margin-767-map-site-mobile">
                    <div class="header-footer">
                        <h3><?php the_field('header-footer-newsletter','option') ?></h3>
                        <!-- <?php echo do_shortcode('[newsletter_form type="minimal"]') ?> -->

                        <div class="tnp tnp-subscription">
                            <form method="post" action="<?php bloginfo('url'); ?>/?na=s"
                                onsubmit="return newsletter_check(this)">
                                <input type="hidden" name="nlang" value="">
                                <input type="hidden" name="nr" value="page">
                                <div class="tnp-field tnp-field-email"><input class="tnp-email" type="email"
                                        placeholder="e-mail" name="ne" required=""></div>
                            </form>
                        </div>


                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="downFooter">
        <div class="container">
            <div class="icon-social-media">
                <?php

// Check rows exists.
if( have_rows('list_icon_socialMedia', 'option') ):

    // Loop through rows.
    while( have_rows('list_icon_socialMedia', 'option') ) : the_row(); ?>




                <a
                    href="<?php the_sub_field('link_do_portalu_footer'); ?>"><?php the_sub_field('item-icon_socialMedia'); ?></a>

                <?php  endwhile;
else :
endif; ?>

            </div>
            <div class="row center-info-footer">
                <div class="col-md-6 col-info-footer-left">Realizacja RedRockS © 2020 </div>
                <div class="col-md-6 col-info-footer-right">Copyright by SIESTA © <?php echo date("Y"); ?>. All rights
                    reserved.</div>
            </div>
        </div>
    </section>
</footer>


<?php wp_footer(); ?>


</body>

</html>