<?php get_header() ?>

<main id="single-post" class="wrapper">


    <section style="background-image: url(<?php the_field('bg_top-lang-filter', 'option') ?>)"
        class="single-post-header">
        <div class="container">
            <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
                <?php
        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb( '<p id="breadcrumbs"><i class="fas fa-home"></i>','</p>' );
        }
    ?>
            </nav>
            <div class="row">
                <div class="col-lg-6">
                    <div class="single-post-title">
                        <div class="center-title-and-bull-course-desc">
                            <h1><?php the_title() ?></h1>
                            <img src="<?php the_field('ikona_obok_tytulu_opis_kursu_siesta') ?>"
                                alt="<?php the_title() ?>">
                        </div>
                    </div>

                    <div class="content-post-wrap">
                        <div class="list_information_course">
                            <?php

// Check rows exists.
if( have_rows('lista_informacji_o_kursie_list') ):

    // Loop through rows.
    while( have_rows('lista_informacji_o_kursie_list') ) : the_row(); ?>


                            <span><?php the_sub_field('informacja_o_kursie_item'); ?></span>



                            <?php  endwhile;


                            else :

                            endif; ?>
                        </div>
                        <?php the_content();?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <?php if ( has_post_thumbnail() ) {
                        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
                        echo '<div style="background: url('. $url.');" class="imgAreaSingle_course">'; ?>
                </div>
                <?php } else { ?>
                <img class="img-course-single-alt" src="<?php the_field('alternative-img-course','option') ?>"
                    alt="<?php the_title(); ?>" />
                <?php } ?>

            </div>
        </div>
        </div>

    </section>


    <section class="contact-bottom-course-contact">
        <div class="container">

            <div class="row">
                <div class="col-lg-6">
                    <h2><?php the_field('alternatywny-title-section-contact-course','option') ?></h2>
                    <div class="form-contact">
                        <?php echo do_shortcode('[contact-form-7 id="281" title="Formularz kontaktowy"]') ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div style="background-image: url(<?php the_field('img-bg-contact','option') ?>)"
                        class="img-bg-contact">
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>


<?php get_footer(); ?>