<?php
 
get_header(); ?>

<div id="course-all-filter-page">
    <?php
 
if ( have_posts() ) :
	?>
    <div class="container">
        <div class="area-title-archive-course-all">
            <h1>Wszystkie kursy wyjazdowe</h1>
        </div>
        <div class="row">


            <?php
	while ( have_posts() ) : the_post(); ?>



            <div class="col-lg-6 col-xl-4">

                <div class="block-lang-filter">
                    <a href="<?php the_permalink(); ?>">

                        <?php 
if ( has_post_thumbnail() ) { ?>
                        <div class="container-img">

                            <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
    echo '<div style="background: url('. $url.');" class="img-post-thumb">'; ?>
                        </div>
                        <div class="overly">
                            <div class="text">
                                <?php echo mb_strimwidth( get_the_excerpt(), 0, 200, '...' ); ?>
                            </div>
                        </div>
                </div>


                <?php } else { ?>


                <div class="container-img">
                    <div style="background: url(<?php the_field('alternative-img-course','option'); ?>);"
                        class="img-post-thumb">;
                    </div>
                    <div class="overly">
                        <div class="text">
                            <?php echo mb_strimwidth( get_the_excerpt(), 0, 200, '...' ); ?>
                        </div>
                    </div>
                </div>
                <?php } ?>



                <h2><?php the_title(); ?></h2>

                <div class="btn-more-filter-area">
                    <div class="btn-red-more"><?php the_field('name_button_more_filter','option') ?></div>
                </div>
                </a>
            </div>
        </div>






        <?php endwhile;

 
endif; ?>

    </div>
</div>
</div>

<?php
get_footer();
 
?>