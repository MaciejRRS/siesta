<?php 
/* 
Template Name: Strona Główna 
*/ 
?>

<?php get_header() ?>

<div class="wrapper">
    <section style="background-image: url(<?php the_field('bg_section_first') ?>)" class="first-section">
        <div class="container">

            <div class="margin-first-section">
                <div class="row center-columns-first-section">
                    <div class="col-md-12 col-lg-5">
                        <div class="first_section_left_block">
                            <div class="title-first-section">
                                <h1><?php the_field('tytul_sekcji_duzy_home') ?></h1>
                                <h2><?php the_field('podtytul_sekcji_home') ?></h2>
                            </div>
                            <div class="desc-section-two">
                                <?php the_field('tekst_zwykly_sekcji_pierwszej') ?>
                            </div>
                            <div class="buttons-first-section">
                                <?php if( get_field('tekst_przycisk_ksiegarnia_home') ): ?>
                                <a href="<?php the_field('link_przycisk_ksiegarnia_home') ?>"
                                    class="btn-siesta-red"><?php the_field('tekst_przycisk_ksiegarnia_home') ?></a>
                                <?php endif; ?>
                                <?php if( get_field('tekst_przycisk_kursyOnline_home') ): ?>
                                <a href="<?php the_field('link_przycisk_kursyOnline_home') ?>"
                                    class="btn-siesta-yellow"><?php the_field('tekst_przycisk_kursyOnline_home') ?></a>
                                <?php endif; ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 col-lg-7">
                        <div class="first_section_right_block">
                            <img src="<?php the_field('first_section_right_block_image') ?>">

                        </div>

                    </div>
                </div>
            </div>

        </div>
        <div class="scrollArea">
            <a href="#section-home-2"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icon/scroll_icon.svg" alt=""
                    class="img-scroll"></a>
        </div>
    </section>

    <section id="section-home-2" class="two-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="left-block-section-two">
                        <img src="<?php the_field('zdjecie_sekcja_druga') ?>">
                    </div>

                </div>
                <div class="col-md-12 col-lg-6">
                    <div class="right-block-section-two">
                        <div class="center-title-and-bull">
                            <h3><?php the_field('title_block_sec-two') ?></h3>
                            <img src="<?php the_field('img_title_bull') ?>" alt="" class="img-title-bull">
                        </div>
                        <div class="desc-section-two">
                            <?php the_field('text_block_section-two') ?>
                        </div>
                        <?php if( get_field('tekst_przycisk_kursyOnline_sec_two_home') ): ?>
                        <a href="<?php the_field('link_przycisk_kursyOnline_sec_two_home') ?>"
                            class="btn-siesta-red"><?php the_field('tekst_przycisk_kursyOnline_sec_two_home') ?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="three-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6">
                    <div class="left-col-section-three">

                        <div class="center-title-and-bull">
                            <h3><?php the_field('title_block_sec-three') ?></h3>
                            <img src="<?php the_field('img_title_bull_sec-three') ?>" alt="" class="img-title-bull">
                        </div>
                        <div class="desc-section-three">
                            <?php the_field('text_block_sec-three') ?>
                        </div>
                        <?php if( get_field('tekst_przycisk_kursyOnline_sec_three_home') ): ?>
                        <a href="<?php the_field('link_przycisk_kursyOnline_section_three_home') ?>"
                            class="btn-siesta-red"><?php the_field('tekst_przycisk_kursyOnline_sec_three_home') ?></a>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="col-md-12 col-lg-6">
                    <img src="<?php the_field('img_bgRight_sec-three') ?>" alt="" class="img_bgRight_sec-three">
                </div>
            </div>
        </div>
    </section>

    <section class="news">
        <div class="container">
            <h3 class="title-news-section"><?php the_field('title_section_news') ?></h3>
            <!-- add News start -->
            <?php get_template_part( 'partials/show_news', 'page' ); ?>
            <!-- add News end -->
        </div>
    </section>

    <section class="last-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-5">
                    <div class="left-col-section-last">
                        <img src="<?php the_field('zdjecie_lewa_kolumna_sec_last') ?>" alt=""
                            class="img-left-col-section-last">
                    </div>
                </div>
                <div class="col-md-12 col-lg-7">
                    <div class="right-col-section-last">

                        <div class="center-title-bullAndText">
                            <h3><?php the_field('title_block_sec-last') ?></h3>
                            <img src="<?php the_field('img_title_bull_sec-last') ?>" alt="" class="img-title-bull-last">
                        </div>
                        <div class="desc-section-last">
                            <?php the_field('text_block_sec-last') ?>
                        </div>
                        <div class="btn-last-section-area">
                            <?php if( get_field('tekst_przycisk_sec_last_home') ): ?>
                            <a href="<?php the_field('link_przycisk_section_last_home') ?>"
                                class="btn-siesta-red"><?php the_field('tekst_przycisk_sec_last_home') ?></a>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <section class="section-info">
        <div class="container">
            <div class="areaTitleAndBull_sec-info">
                <img src="<?php the_field('img_title_logo_sec-info') ?>" alt="Szkoła Językowa Siesta"
                    class="img_title_logo_sec-info">
                <h3 class="title-section-info"><?php the_field('title-section-info_home') ?></h3>
            </div>

            <div class="list-contact">
                <div class="row">
                    <?php

// Check rows exists.
if( have_rows('list-contact-home') ):

    // Loop through rows.
    while( have_rows('list-contact-home') ) : the_row(); ?>

                    <div class="col-md-12 col-lg-4">

                        <div class="area_icon_text_contact_home">
                            <div class="icon-contact-area">
                                <img class="img-contact-home" src=" <?php  the_sub_field('img-contact-home'); ?>"
                                    alt="">
                            </div>
                            <div class="text-contact-area">
                                <?php  the_sub_field('text-contact-home'); ?>
                            </div>
                        </div>

                    </div>

                    <?php // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
                </div>
            </div>
        </div>
    </section>
</div>


<?php get_footer() ?>