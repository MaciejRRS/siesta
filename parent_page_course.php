<?php
/*
 Template Name: Strona rodzic - Kursy
 */
?>
<?php get_header() ?>

<section class="first-section">
    <div class="container">
        <div class="margin-first-section">
            <nav class="breadcrumb d-flex align-items-center" aria-label="breadcrumb">
                <?php
					if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
					}
				?>
            </nav>


            <?php
$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID, // Get this pages id and find the children
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );
 
 
$parent = new WP_Query( $args );
 
if ( $parent->have_posts() ) : ?>


            <div class="container">
                <div class="row">
                    <?php while ( $parent->have_posts() ) : $parent->the_post();

   ?>


                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							echo '<div style="background: url('. $url.'); background-repeat:no-repeat; background-position:center; background-size:cover;" class="col-gallerymasonry-flex">'; ?>
                        <a class="block-click" href="<?php echo get_permalink( $post->ID ); ?>">
                            <div class="text-area-gallerymasonry">
                                <h2 class="title-col-gallerymasonry">
                                    <?php the_title(); ?></h2>
                            </div>
                        </a>
                    </div>
                </div>

                <?php endwhile; ?>
            </div>

        </div>


        <?php endif; wp_reset_query(); ?>

    </div>






    </div>
    </div><!-- .container -->
</section>









<?php get_footer('subpage'); ?>