<?php
/*
 Template Name: Aktualności
 */
?>
<?php
get_header(); ?>
<main id="course-all" role="main">
    <div class="wrapper">



        <section style="background-image: url(<?php the_field('bg_top-lang-filter','option') ?>)" class="welcome-text">
            <div class="container">
                <div class="row">
                    <div data-aos="fade-right" data-aos-duration="1500" class="col-lg-6">
                        <div class="area-titleAndBull">
                            <h1 class="title-page-courses"><?php the_title(); ?></h1>
                            <img src="<?php the_field('bull_news_title') ?>" alt="" class="bull-courdes-title">
                        </div>
                        <div class="text-area-description-course">
                            <?php the_content(); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div style="background-image: url(<?php the_field('zdjecie_obok_glownego_tekstu_news') ?>)"
                            class="img-area-description-course">
                        </div>
                    </div>
                </div>
            </div>
            <div class="scrollArea">
                <a href="#section-choose-course-online"><img
                        src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icon/scroll_icon.svg" alt=""
                        class="img-scroll"></a>
            </div>
        </section>





















    </div> <!-- end wrapper -->



    <?php
// Protect against arbitrary paged values
$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;


 
$args = array(
    'post_type' => 'news',
    'post_status'=>'publish',
    'paged' => $paged,
);

$the_query = new WP_Query($args);
?>

    <?php if ( $the_query->have_posts() ) : ?>


    <section id="section-choose-course-online" data-aos="zoom-in" data-aos-duration="1500" class="news">
        <div class="container">
            <div class="blocks-news-area">
                <div class="row margin-top-75">

                    <!-- the loop -->
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>


                    <div class="col-lg-4 col-md-6">

                        <div class="news-item">
                            <a href="<?php echo get_permalink(); ?>">
                                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                echo '<div style="background: url('. $url.');" class="bg-post-img">'; ?>
                        </div>

                        <div class="news-desc-area">

                            <h3 class="news-title-post"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?>
                            </h3>
                            <div class="excerptNewsArea">
                                <p class="news-description">
                                    <?php echo wp_trim_words( get_the_excerpt(), 16, '...' ); ?></p>
                            </div>
                            </a>

                        </div>
                        <div class="button-news-area">
                            <a href="<?php echo get_permalink(); ?>"
                                class="btn-more-news"><?php the_field('button_read_more_news','option') ?></a>
                        </div>
                    </div>






                </div>




                <?php endwhile; ?>
                <!-- end of the loop -->

                <?php
wp_reset_query();
?>




            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="pagination">
                    <?php
								echo paginate_links( array(
									'format'  => 'page/%#%',
									'current' => $paged,
									'total'   => $the_query->max_num_pages,
									'mid_size'        => 2,
									'prev_text'       => __('&laquo;  Cofnij'),
									'next_text'       => __('Dalej  &raquo;')
								) );
							?>
                </div>

                <?php endif; ?>
            </div>
        </div>
        </div>
    </section>











</main><!-- .site-main -->
<?php get_footer(); ?>