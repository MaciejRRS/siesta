<?php 
/* 
Template Name: Strona O nas
*/ 
?>

<?php get_header() ?>

<div id="o_nas" class="wrapper">
    <section style="background-image: url(<?php the_field('bg_section_about') ?>)" class="first-section-about">
        <div class="container">
            <div class="margin-first-section-about">
                <div class="row center-columns-first-section">
                    <div class="col-lg-6">
                        <div data-aos="fade-right" data-aos-duration="1500" class="first_section_left_block">
                            <div class="center-title-and-bull">
                                <h2><?php the_field('title_section_first_about') ?></h2>
                                <img src="<?php the_field('img_title_bull_first_about') ?>" alt=""
                                    class="img-title-bull">
                            </div>
                            <div class="desc-section-first-about">
                                <?php the_content(); ?>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="first_section_right_block">
                            <img src="<?php the_field('first_section_right_block_image_about') ?>">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="scrollArea">
            <a href="#section-about-2"><img
                    src="<?php echo get_stylesheet_directory_uri(); ?>/assets/icon/scroll_icon.svg" alt=""
                    class="img-scroll"></a>
        </div>
    </section>

    <section id="section-about-2" class="sec-two-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="left-col-two-sec-aboutArea">
                        <div style="background-image: url(<?php the_field('img-left-sec-two-about') ?>)"
                            class="bg-left-col-about-two"></div>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div data-aos="fade-left" data-aos-duration="1500" class="right-col-two-sec-about">
                        <div class="center-titleAndBull-about">
                            <h2><?php the_field('title-two-sec-about') ?></h2>
                            <img src="<?php the_field('img-bull-about-title-secTwo') ?>" alt="">
                        </div>
                        <div class="text-area-sec-two">
                            <?php the_field('text-two-sec-about') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section style="background-image: url(<?php the_field('bg_section_about') ?>)" class="sec-three-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div data-aos="fade-right" data-aos-duration="1500" class="left-col-three-sec-about">
                        <div class="center-titleAndBull-about">
                            <h2><?php the_field('title-three-sec-about') ?></h2>
                            <img src="<?php the_field('img-bull-about-title-secThree') ?>" alt="">
                        </div>
                        <div class="text-area-sec-three">
                            <?php the_field('text-three-sec-about') ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right-col-three-sec-about">
                        <div style="background-image: url(<?php the_field('img-left-sec-three-about') ?>)"
                            class="bg-right-col-about-three"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section data-aos="zoom-in" data-aos-duration="1500" class="news">
        <div class="container">
            <h3 class="title-news-section"><?php the_field('title_section_posts') ?></h3>
            <!-- add News start -->
            <?php get_template_part( 'partials/show_posts', 'page' ); ?>
            <!-- add News end -->
        </div>
    </section>
</div>


<?php get_footer() ?>