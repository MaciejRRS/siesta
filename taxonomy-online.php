<?php get_header();?>



<div id="course-all-filter-page">
    <div class="wrapper">

        <section style="background-image: url('<?php the_field('bg_top-lang-filter','option') ?>')"
            class="top-lang-filter">
            <div class="container">

                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="text-area-desc-cat">
                            <div class="area-titleAndBull-cat">

                                <!-- title  -->
                                <?php 
                                    $current_term = get_queried_object(); 
                                    $title_category_custom = get_field('tytul_kursu_category_custom_post', $current_term );
                                    echo '<h1>'.$title_category_custom.'<h1>';
                                    ?>
                                <!-- title end -->


                                <img src="<?php the_field('ikona_przy_tytule_kategorii_byczek','option') ?>">
                            </div>
                            <?php echo category_description(); ?>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="img-flag-topFilter">


                            <!--  -->
                            <?php 
$current_term = get_queried_object(); 
$image = get_field('miniatura_kategorii_custom_post', $current_term );
echo '<img src='.$image.'>';
?>

                            <!--  -->

                        </div>
                    </div>
                </div>
            </div>
        </section>




        <div class="container">

            <!-- Filtr dla kursów online języka angielskiego -->
            <?php if ( is_tax( 'online', 'angielski' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="298"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="298" show="results"]') ?>
            <?php }?>

            <!-- Filtr dla kursów online języka argentyńskiego -->
            <?php if ( is_tax( 'online', 'portugalski' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="303"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="303" show="results"]') ?>
            <?php }?>

            <!-- Filtr dla kursów online języka francuskiego -->
            <?php if ( is_tax( 'online', 'francuski' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="306"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="306" show="results"]') ?>
            <?php }?>

            <!-- Filtr dla kursów online języka hiszpańskiego -->
            <?php if ( is_tax( 'online', 'hiszpanski' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="307"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="307" show="results"]') ?>
            <?php }?>

            <!-- Filtr dla kursów online języka niemieckiego -->
            <?php if ( is_tax( 'online', 'niemiecki' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="308"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="308" show="results"]') ?>
            <?php }?>

            <!-- Filtr dla kursów online języka włoskiego -->
            <?php if ( is_tax( 'online', 'wloski' ) ) { ?>
            <?php echo do_shortcode('[searchandfilter id="309"]') ?>
            <?php echo do_shortcode ('[searchandfilter id="309" show="results"]') ?>
            <?php }?>

        </div>
        <section class="contact-bottom-course">
            <div class="container">
                <h2><?php the_field('title-section-contact-course','option') ?></h2>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-contact">
                            <?php echo do_shortcode('[contact-form-7 id="281" title="Formularz kontaktowy"]') ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div style="background-image: url(<?php the_field('img-bg-contact','option') ?>)"
                            class="img-bg-contact">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>




<?php get_footer(); ?>