<div class="blocks-news-area">
    <div class="row">


        <?php 
   // the query
   $the_query = new WP_Query( array(
    'post_type' => 'post',
    'post_status'=>'publish',
      'posts_per_page' => 3,
   )); 
    ?>

        <?php if ( $the_query->have_posts() ) : ?>
        <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
        <div class="col-md-4">

            <div class="news-item">
                <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                            echo '<div style="background: url('. $url.');" class="bg-post-img">'; ?>
            </div>

            <div class="news-desc-area">
                <a href="<?php echo get_permalink(); ?>">
                    <h3 class="news-title-post"><?php echo wp_trim_words( get_the_title(), 10, '...' ); ?></h3>
                    <div class="excerptNewsArea">
                        <p class="news-description"><?php echo wp_trim_words( get_the_excerpt(), 16, '...' ); ?></p>
                    </div>
                </a>

            </div>
            <div class="button-news-area">
                <a href="<?php echo get_permalink(); ?>"
                    class="btn-more-news"><?php the_field('button_read_more_news','option') ?></a>
            </div>
        </div>






    </div>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>

    <?php else : ?>
    <p><?php __('No News'); ?></p>
    <?php endif; ?>
</div>
</div>