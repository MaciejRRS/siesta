// animated burger
$(document).ready(function () {
    $('.first-button').on('click', function () {
        $('.animated-icon1').toggleClass('open');
    });
    $('.second-button').on('click', function () {
        $('.animated-icon2').toggleClass('open');
    });
    $('.third-button').on('click', function () {
        $('.animated-icon3').toggleClass('open');
    });
    $('#nav-icon0,#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4,#nav-icon5').click(function () {
        $(this).toggleClass('open');
    });
});


//scroll top nav addClas/removeClass
$(window).on(`scroll`, function() {
    if ($(window).scrollTop()) {
        $(`nav.navbar`).addClass(`navbar-box-shadow`);
        $(`section.slider`).addClass(`scroll`);
    } else {
        $(`nav.navbar`).removeClass(`navbar-box-shadow`);
        $(`section.slider`).removeClass(`scroll`);
    }
});
