<?php

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';




// function shapeSpace_include_custom_jquery() {

// 	wp_deregister_script('jquery');
//     wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js', array(), null, true);
    
// $theme = wp_get_theme();

// wp_enqueue_style('Style',get_template_directory_uri().'/css/styles.css',array(),$theme->get('Version'),'all');
// }

// add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');





function add_stylesheets_and_scripts()
{
wp_enqueue_style( 'style', get_stylesheet_uri(), array(), filemtime(get_template_directory() . '/style.css'), 'all' );
wp_enqueue_style('custom', get_template_directory_uri() . '/assets/dist/css/main.min.css', array(), filemtime(get_template_directory() . '/assets/dist/css/main.min.css'), 'all' );

wp_deregister_script('jquery');
wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/src/js/libr/jquery.min.js', array(), null, true);
wp_enqueue_script('bootstrap', get_template_directory_uri() . '/assets/src/js/libr/bootstrap.min.js', array(), null, true);
wp_enqueue_script('popper', get_template_directory_uri() . '/assets/src/js/libr/popper.min.js', array(), null, true);
wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/src/js/main.js', array(), null, true );
wp_enqueue_script('fontawesome', get_template_directory_uri() . '/assets/src/js/fontawesome.js', array(), null, true);

// add scripts on specific webpages
global $template;

	if ( ( basename( $template ) !== 'hompage.php' ) ) {
    wp_enqueue_style( 'aos', get_template_directory_uri() . '/assets/src/css/aos.css' );
	wp_enqueue_script( 'aos-start', get_template_directory_uri() . '/assets/src/js/aos-start.js', array(), null, true );
    wp_enqueue_script( 'aos-js', get_template_directory_uri() . '/assets/src/js/aos.js', array(), null, true );
}

}
add_action('wp_enqueue_scripts', 'add_stylesheets_and_scripts');










register_nav_menus( array(
	'primary-menu' => __( 'Primary Menu', 'siesta' ),
) );

// add Yoast SEO
function get_breadcrumb() {
	echo '<a href="'.home_url().'" rel="nofollow">Home</a>';
	if (is_category() || is_single()) {
		echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
		the_category(' &bull; ');
			if (is_single()) {
				echo " &nbsp;&nbsp;&#187;&nbsp;&nbsp; ";
				the_title();
			}
	} elseif (is_page()) {
		echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;";
		echo the_title();
	} elseif (is_search()) {
		echo "&nbsp;&nbsp;&#187;&nbsp;&nbsp;Search Results for... ";
		echo '"<em>';
		echo the_search_query();
		echo '</em>"';
	}
}




// register sidebar
function enable_widgets() {

    register_sidebar(
        array(
            'name' => 'Main Sidebar',
            'id'   => 'sidebar-1',
            'description'   => 'Here you can add widgets to the main sidebar.',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h5 id="widget-heading">',
            'after_title'   => '</h5>'
    ));
 }

 add_action('widgets_init','enable_widgets');





 /**
 * Change several of the breadcrumb defaults
 */
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
            'delimiter'   => ' &gt; ',
            'wrap_before' => '<nav class="woocommerce-breadcrumb" itemprop="breadcrumb">',
            'wrap_after'  => '</nav>',
            'before'      => '',
            'after'       => '',
            'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
        );
}

// Delite margin-top:32px from html
// add_filter(  'show_admin_bar' , '__return_false' );

/*
* Enable support for custom logo.
*
*/
	add_theme_support( 'custom-logo', array(
		'height'      => 400,
		'width'       => 100,
		'flex-height' => true,
		'flex-width'  => true,
	) );







// Register thumbnails
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'homepage-thumb', 385, 302 ); // Soft Crop Mode





// This adds support for pages only:
	add_theme_support( 'post-thumbnails', array( 'page' ) );





//add option page to panel (ACF)
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
	
}





// // Aktualności
// // Dodawanie nowego typu postów START 
// // add the filter
// // add_filter( 'acf_the_content', 'filter_acf_the_content', 10, 1 );

function create_news_posttype(){
	$args = array(
		'label' => 'Aktualności',
		'labels' => array(
			'name' => __('Aktualności'),
			'singular_name' => __('Artykuł'),
			'add_new' => __('Dodaj Artykuł'),
			'add_new_item' => __('Dodaj Nowy Artykuł'),
			'edit_item' => __('Edytuj Artykuł'),
			'new_item' => __('Nowy Artykuł'),
			'view_item' => __('Zobacz Artykuł'),
			'search_items' => __('Szukaj Artykułów'),
			'not_found' => __('Nie Znaleziono Artykułów'),
			'not_found_in_trash' => __('Brak Artykułów w koszu'),
			'all_items' => __('Wszystkie Artykuły'),
			'archives' => __('Zarchiwizowane Artykuły'),
			'insert_into_item' => __('Dodaj do Artykułu'),
			'uploaded_to_this_item' => __('Sciągnięto do bieżącego Artykułu'),
			'featured_image' => __('Zdjęcie Artykułu'),
			'set_featured_image' => __('Ustaw Zdjęcie Artykułu'),
			'remove_featured_image' => __('Usuń Zdjęcie Artykułu'),
			'use_featured_image' => __('Użyj Zdjęcie Artykułu'),
			'menu_name' => __('Aktualności')
		),
		'description' => __('Typ Postu zawiera treść dla Artykułu'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 6,
		'menu_icon' => 'dashicons-book-alt',
		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
		'has_archive' => true,
		'hierarchical' => true,
		'show_in_rest' 		=> true,
		'rewrite' => array('slug'=>'news','with_front'=>false),
		'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		),
	);
	register_post_type('news',$args);
}
add_action('init','create_news_posttype',0);

function register_taxonomy_category_news() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Category'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'category_news',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('category_news', array('news'), $args);
}
add_action('init', 'register_taxonomy_category_news');

// Dodawanie nowego typu postów END






// *****************************


// // Kursy wyjazdowe
// // Dodawanie nowego typu postów START 
// // add the filter
// // add_filter( 'acf_the_content', 'filter_acf_the_content', 10, 1 );

function create_kursy_wyjazdowe_posttype(){
	$args = array(
		'label' => 'Kursy wyjazdowe',
		'labels' => array(
			'name' => __('Kursy wyjazdowe'),
			'singular_name' => __('kurs wyjazdowy'),
			'add_new' => __('Dodaj kurs wyjazdowy'),
			'add_new_item' => __('Dodaj nowy kurs wyjazdowy'),
			'edit_item' => __('Edytuj kurs wyjazdowy'),
			'new_item' => __('Nowy kurs wyjazdowy'),
			'view_item' => __('Zobacz kurs wyjazdowy'),
			'search_items' => __('Szukaj kursów wyjazdowych'),
			'not_found' => __('Nie Znaleziono kursów wyjazdowych'),
			'not_found_in_trash' => __('Brak kursów wyjazdowych w koszu'),
			'all_items' => __('Wszystkie kursy wyjazdowe'),
			'archives' => __('Zarchiwizowane kursy wyjazdowe'),
			'insert_into_item' => __('Dodaj do kursu wyjazdowego'),
			'uploaded_to_this_item' => __('Sciągnięto do bieżącego kursu wyjazdowego'),
			'featured_image' => __('Zdjęcie kursu wyjazdowego'),
			'set_featured_image' => __('Ustaw Zdjęcie kursu wyjazdowego'),
			'remove_featured_image' => __('Usuń Zdjęcie kursu wyjazdowego'),
			'use_featured_image' => __('Użyj Zdjęcia kursu wyjazdowego'),
			'menu_name' => __('Kursy wyjazdowe')
		),
		'description' => __('Typ Postu zawiera treść dla Kursu wyjazdowego'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 6,
		'menu_icon' => 'dashicons-book-alt',
		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
		'has_archive' => true,
		'hierarchical' => true,
		'show_in_rest' 		=> true,
		'rewrite' => array('slug'=>'kursywyjazdowe','with_front'=>false),
		'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		),
	);
	register_post_type('kursywyjazdowe',$args);
}
add_action('init','create_kursy_wyjazdowe_posttype',0);

function register_taxonomy_category_kursy_wyjazdowe() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Category'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'wyjazdowe',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('wyjazdowe', array('kursywyjazdowe'), $args);
}
add_action('init', 'register_taxonomy_category_kursy_wyjazdowe');

// Dodawanie nowego typu postów END








function register_taxonomy_category_oldyear_atr() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Dodaj nowy atrybut'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Atrybuty wiek'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'grupa_wiekowa',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('category_oldyear_atr', array('kursywyjazdowe'), $args);
}
add_action('init', 'register_taxonomy_category_oldyear_atr');
















// *****************************

// // Kursy online
// // Dodawanie nowego typu postów START 
// // add the filter
// // add_filter( 'acf_the_content', 'filter_acf_the_content', 10, 1 );

function create_kursy_online_posttype(){
	$args = array(
		'label' => 'Kursy online',
		'labels' => array(
			'name' => __('Kursy online'),
			'singular_name' => __('kurs online'),
			'add_new' => __('Dodaj kurs online'),
			'add_new_item' => __('Dodaj nowy kurs online'),
			'edit_item' => __('Edytuj kurs online'),
			'new_item' => __('Nowy kurs online'),
			'view_item' => __('Zobacz kurs online'),
			'search_items' => __('Szukaj kursów online'),
			'not_found' => __('Nie Znaleziono kursów online'),
			'not_found_in_trash' => __('Brak kursów online w koszu'),
			'all_items' => __('Wszystkie kursy online'),
			'archives' => __('Zarchiwizowane kursy online'),
			'insert_into_item' => __('Dodaj do kursu online'),
			'uploaded_to_this_item' => __('Sciągnięto do bieżącego kursu online'),
			'featured_image' => __('Zdjęcie kursu online'),
			'set_featured_image' => __('Ustaw Zdjęcie kursu online'),
			'remove_featured_image' => __('Usuń Zdjęcie kursu online'),
			'use_featured_image' => __('Użyj Zdjęcia kursu online'),
			'menu_name' => __('Kursy online')
		),
		'description' => __('Typ Postu zawiera treść dla Kursu online'),
		'public' => true,
		'exclude_from_search' => false,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 6,
		'menu_icon' => 'dashicons-book-alt',
		'supports' => array('title','editor','author','thumbnail','excerpt','revisions','page-attributes'),
		'has_archive' => true,
		'hierarchical' => true,
		'show_in_rest' 		=> true,
		'rewrite' => array('slug'=>'kursyonline','with_front'=>false),
		'capabilities' => array(
			'edit_post'          => 'update_core',
			'read_post'          => 'update_core',
			'delete_post'        => 'update_core',
			'edit_posts'         => 'update_core',
			'edit_others_posts'  => 'update_core',
			'delete_posts'       => 'update_core',
			'publish_posts'      => 'update_core',
			'read_private_posts' => 'update_core'
		),
	);
	register_post_type('kursyonline',$args);
}
add_action('init','create_kursy_online_posttype',0);

function register_taxonomy_category_kursy_online() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Add New Category'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Category'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'online',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('online', array('kursyonline'), $args);
}
add_action('init', 'register_taxonomy_category_kursy_online');

// Dodawanie nowego typu postów END


function register_taxonomy_category_oldyear_atr_online() {
    $labels = [
        'name'              => _x('Categories', 'taxonomy general name'),
        'singular_name'     => _x('Category', 'taxonomy singular name'),
        'search_items'      => __('Search Categories'),
        'all_items'         => __('All Categories'),
        'parent_item'       => __('Parent Category'),
        'parent_item_colon' => __('Parent Category:'),
        'edit_item'         => __('Edit Category'),
        'update_item'       => __('Update Category'),
        'add_new_item'      => __('Dodaj nowy atrybut'),
        'new_item_name'     => __('New Category Name'),
        'menu_name'         => __('Atrybuty wiek'),
        ];
        $args = [
        'hierarchical'      => true, // make it hierarchical (like categories)
        'labels'            => $labels,
        'show_ui'           => true,
		'show_admin_column' => true,
		'show_in_rest' 		=> true,
		'query_var'         => true,
		'has_archive'		=> true,
		'rewrite'           => ['slug' => 'grupa_wiekowa',
								'hierarchical' => true,
								'with_front' => true],
        ];
        register_taxonomy('category_oldyear_atr_online', array('kursyonline'), $args);
}
add_action('init', 'register_taxonomy_category_oldyear_atr_online');






// Custom login page
function login_p() { ?>
<style type="text/css">
#login h1 a,
.login h1 a {
    background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/login_page/logo.svg');
    background-size: contain !important;
    width: 320px;
    max-width: 100%;
    background-size: 320px auto;
    background-repeat: no-repeat;
    padding-bottom: 0px;
}

body {
    background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/login_page/background.svg') !important;
}

#loginform {
    background: #da0021;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
    color: #fff;
    font-family: 'Baloo Thambi 2', cursive;
}

.button-primary {
    background: #FFD400 !important;
    border: 1px solid #FFD400 !important;
    padding: 10px;
    color: #fff;
    text-decoration: none;
    font-family: 'Baloo Thambi 2', cursive;
    font-weight: 400;
    text-transform: uppercase;
    transition: .3s;
    border: 2px solid #FFD400;
    font-size: 27px;
    box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);
    border-radius: 0 !important;
}

.button-primary:hover {
    background: #fff !important;
    color: #FFD400 !important;
}

.privacy-policy-link {
    color: #aaa !important;
}

.login form .input,
.login input[type=password],
.login input[type=text] {
    border-radius: 0;
}

input[type=checkbox]:focus,
input[type=color]:focus,
input[type=date]:focus,
input[type=datetime-local]:focus,
input[type=datetime]:focus,
input[type=email]:focus,
input[type=month]:focus,
input[type=number]:focus,
input[type=password]:focus,
input[type=radio]:focus,
input[type=search]:focus,
input[type=tel]:focus,
input[type=text]:focus,
input[type=time]:focus,
input[type=url]:focus,
input[type=week]:focus,
select:focus,
textarea:focus {
    border-color: #da0021 !important;
    box-shadow: 0 0 0 1px #da0021 !important;
    outline: 2px solid transparent !important;
}
</style>
<?php }
	add_action( 'login_enqueue_scripts', 'login_p' );