<?php 
/* 
Template Name: Kontakt
*/ 
?>

<?php get_header() ?>

<div id="contact_us" class="wrapper">
    <section style="background-image: url(<?php the_field('bg_section_contact') ?>)" class="first-section-about">
        <div class="container">
            <div class="margin-sec-contact">
                <div class="row">
                    <div class="col-lg-6">
                        <div data-aos="fade-right" data-aos-duration="1500" class="sec-contac_left_block">
                            <div class="center-title-and-bull-contact">
                                <h1><?php the_title(); ?></h1>
                                <img src="<?php the_field('img_title_bull_contact') ?>" alt=""
                                    class="img-title-bull-contact">
                            </div>
                            <div class="text-name-title-contact">
                                <h3><?php the_field('nazwa_szkoly_contact') ?></h3>
                                <div class="row">
                                    <?php

// Check rows exists.
if( have_rows('textAndIcon-contact-item-list') ):
    // Loop through rows.
    while( have_rows('textAndIcon-contact-item-list') ) : the_row(); ?>
                                    <div class="col-sm-6">
                                        <div class="area_contact-item">
                                            <div class="icon-contact">
                                                <img src="<?php the_sub_field('icon-contact-item'); ?>">
                                            </div>
                                            <div class="text-contact">
                                                <?php the_sub_field('text-contact-item'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
    // End loop.
    endwhile;

// No value.
else :
    // Do something...
endif; ?>
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="sec-contac_right_block">
                            <img src="<?php the_field('first_section_right_block_image_about') ?>">
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>


</div>


<?php get_footer() ?>